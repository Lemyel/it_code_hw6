from django.shortcuts import render

# Create your views here.

def index(request):
    return render(request, 'books/index.html')

def about(request):
    return render(request, 'books/about_site.html')